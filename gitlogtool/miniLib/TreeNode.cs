﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace miniLib
{
    public class TreeItem : INotifyPropertyChanged
    {
        private string _fullPath;
        private string _name;
        private string _parentFolder;
        private SuspendableObservableCollection<ICommitItem> _commitFiles;
        private IEnumerable<TreeItem> _children;

        public TreeItem(string _fullPath)
        {
            FullPath = _fullPath;
            ParentFolder = Path.GetDirectoryName(FullPath);
            Name = Path.GetFileName(FullPath);
        }
        public string FullPath
        {
            get => _fullPath;
            set
            {
                if (_fullPath == value)
                    return;
                _fullPath = value;
                OnPropertyChanged("FullPath");
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (_name == value)
                    return;
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        public string ParentFolder
        {
            get => _parentFolder;
            set
            {
                if (_parentFolder == value)
                    return;
                _parentFolder = value;
                OnPropertyChanged("ParentFolder");
            }
        }
        public SuspendableObservableCollection<ICommitItem> CommitFiles
        {
            get => _commitFiles;
            set
            {
                if (_commitFiles == value)
                    return;
                _commitFiles = value;
                OnPropertyChanged("CommitFiles");
            }
        }

        public IEnumerable<TreeItem> Children
        {
           
            get => _children;
            set
            {
                if (_children == value)
                    return;
                _children = value;
                OnPropertyChanged("Children");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class TreeFolderItem : TreeItem
    {
        public TreeFolderItem(string _fullPath) : base(_fullPath)
        {
        }
    }
    public class TreeFileItem : TreeItem
    {
        public TreeFileItem(string _fullPath) : base(_fullPath)
        {
        }
    }
}
