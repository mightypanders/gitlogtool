﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace miniLib
{
    public class SuspendableObservableCollection<T> : ObservableCollection<T>
    {
        private bool _SuspendChangedNotification = false;

        public SuspendableObservableCollection()
        {
        }

        public SuspendableObservableCollection(List<T> list) : base(list)
        {
        }

        public SuspendableObservableCollection(IEnumerable<T> collection) : base(collection)
        {
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!_SuspendChangedNotification)
                base.OnCollectionChanged(e);
        }

        public void AddRange(IEnumerable<T> list,int n)
        {
            if (list == null)
                throw new ArgumentNullException("Incoming list may not be null");
            _SuspendChangedNotification = true;
            this.CheckReentrancy();
            foreach (T item in list)
            {
                Add(item);
            }
            _SuspendChangedNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

        }
        public void AddRange(IEnumerable<T> list)
        {
            if (list == null)
                throw new ArgumentNullException("Incoming list may not be null");
            _SuspendChangedNotification = true;
            foreach (T item in list)
            {
                this.Add(item);
            }
            _SuspendChangedNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

        }
    }
}
