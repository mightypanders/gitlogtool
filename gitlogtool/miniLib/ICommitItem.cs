﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace miniLib
{
    public interface ICommitItem 
    {
        string this[string columnName] { get; }

        string CommitAuthor { get; set; }
        DateTime CommitDate { get; set; }
        bool validItem { get; }
        bool? CommitDocumented { get; set; }
        string CommitFileFullPath { get; }
        string CommitFileName { get; set; }
        string CommitFileDisplayName { get; set; }
        string CommitFilePath { get; set; }
        string CommitMessage { get; set; }
        string CommitSHA { get; set; }
        DateTime DocumentedDate { get; set; }
        string DocumentedVersion { get; set; }
        string DocumentingUser { get; set; }
        string Error { get; }
        string ParentDirectory { get; }
        string ShortSHA { get; }
        string Parent { get; set; }
        string Key { get; set; }

        event PropertyChangedEventHandler PropertyChanged;

        void Clear();
        void Dispose();
        void OnPropertyChanged([CallerMemberName] string propertyName = null);
        string ToString();
    }

}