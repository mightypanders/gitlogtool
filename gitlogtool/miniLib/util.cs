﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace miniLib
{
    public static class util
    {
        /// <summary>
        /// Checks wether git is installed in the default location on the machine.
        /// </summary>
        /// <returns>True if git is found. False if not.</returns>
        public static bool checkForGit()
        {
            return File.Exists("C:\\Program Files\\Git\\git-bash.exe");
        }
        public static bool isGitRepo(string WorkingPath)
        {
            bool? exists = false;
            try
            {
                exists = Directory.Exists(Path.Combine(WorkingPath, ".git"));

            }
            catch (Exception ex)
            {
                throw ex;

            }
            return exists.Value;
        }
        /// <summary>
        /// Execute a git command in the given workingpath. Does not sanitycheck for valid path.
        /// </summary>
        /// <param name="inputCommand">Commands for git. Ex.: log, ls-remote, rev-parse</param>
        /// <param name="WorkingPath">The path git should use for operation</param>
        /// <returns>Output value that is delivered by the git command</returns>
        public static string doGitCommand(string inputCommand, string WorkingPath)
        {
            // https://msdn.microsoft.com/de-de/library/system.diagnostics.processstartinfo.useshellexecute(v=vs.110).aspx

            string outString = "";
            Process cmd = new Process();
            cmd.StartInfo.WorkingDirectory = WorkingPath;
            cmd.StartInfo.FileName = "C:\\Program Files\\Git\\bin\\git.exe";
            cmd.StartInfo.UseShellExecute = false;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.Arguments = inputCommand;
            try
            {
                cmd.Start();
                outString += cmd.StandardOutput.ReadToEnd();
                cmd.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There has been a problem with the git command execution:");
                Console.WriteLine(ex.Message);
            }
            return outString;
        }
        public static string convertUmlaut(string strIn)
        {
            string strOut = strIn;
            strOut = strOut.Replace("\\303\\237", "ß");
            strOut = strOut.Replace("ÃŸ", "ß");
            strOut = strOut.Replace("\\303\\244", "ä");
            strOut = strOut.Replace("\\303\\204", "ä");
            strOut = strOut.Replace("Ã¤", "ä");
            strOut = strOut.Replace("Ã„", "Ä");
            strOut = strOut.Replace("\\303\\274", "ü");
            strOut = strOut.Replace("\\303\\234", "Ü");
            strOut = strOut.Replace("Ã¼", "ü");
            strOut = strOut.Replace("Ãœ", "Ü");
            strOut = strOut.Replace("\\303\\266", "ö");
            strOut = strOut.Replace("\\303\\226", "Ö");
            strOut = strOut.Replace("Ã¶", "ö");
            strOut = strOut.Replace("Ã–", "Ö");
            return strOut;
        }
    }
}
