﻿using DevExpress.Mvvm.UI.Interactivity;
using DevExpress.Xpf.Grid;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace gitlogtool.ui
{
    public class DeferredColumnFitBehavior : Behavior<GridControl>
    {
        private Task _task;
        GridControl AssociatedGrid { get { return AssociatedObject; } }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedGrid.ItemsSourceChanged += OnItemsSourceChanged;
        }

        protected override void OnDetaching()
        {
            AssociatedGrid.ItemsSourceChanged -= OnItemsSourceChanged;
            base.OnDetaching();
        }

        private void OnItemsSourceChanged(object sender, ItemsSourceChangedEventArgs e)
        {
            BestFit();
        }


        private void BestFit()
        {
            //Dispatcher.BeginInvoke(new Action(() => ((TreeListView)AssociatedGrid.View).BestFitColumns()));
            if (_task != null && !_task.IsCompleted) return;
            if (AssociatedGrid.View.GetType() == typeof(TreeListView))
                _task = Task.Factory
                    .StartNew(() => Thread.Sleep(500))
                    .ContinueWith(t => ((TreeListView)AssociatedGrid.View).BestFitColumns(),
                        TaskScheduler.FromCurrentSynchronizationContext());
            else if (AssociatedGrid.View.GetType() == typeof(TableView))
                _task = Task.Factory
                    .StartNew(() => Thread.Sleep(500))
                    .ContinueWith(t => ((TableView)AssociatedGrid.View).BestFitColumns(),
                        TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}