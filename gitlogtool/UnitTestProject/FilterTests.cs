﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using gitlogtool.logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using miniLib;

namespace gitlogtool.logic.Tests
{
    [TestClass()]
    public class FilterTests
    {
        [TestMethod()]
        public void ApplyExludeFilterListTest()
        {
            //Initialisierung der benötigten Laufzeitvariablen
            Filter filter = new Filter();
            filter.addExcludeFilterToList("test1");
            List<ICommitItem> UnfilteredCommitList = new List<ICommitItem>
            {
                new CommitItem("1234", "testauthor", DateTime.Now, "this should pass", "test", false),
                new CommitItem("1234", "testauthor", DateTime.Now, "this should pass test", "test", false),
                new CommitItem("1234", "testauthor", DateTime.Now, "this should not pass test1", "test", false)
            };
            List<ICommitItem> FilteredCommitList = new List<ICommitItem>();
            //Ausführen des zu testenden Befehls
            FilteredCommitList = filter.ApplyExludeFilterList(UnfilteredCommitList);
            //Sicherstellen, dass der Befehl korrekt ausgeführt wurde
            Assert.AreNotEqual(UnfilteredCommitList, FilteredCommitList);
            Assert.AreEqual(FilteredCommitList.Count, 2);
            Assert.AreEqual(FilteredCommitList[0].CommitMessage,"this should pass");
            Assert.AreEqual(FilteredCommitList[1].CommitMessage,"this should pass test");
        }

        [TestMethod]
        public void ApplyIncludeFilterListTest()
        {
            //Initialisierung der benötigten Laufzeitvariablen
            Filter filter = new Filter();
            filter.addIncludeFilterToList("test1");
            List<ICommitItem> UnfilteredCommitList = new List<ICommitItem>
            {
                new CommitItem("1234", "testauthor", DateTime.Now, "this should not pass", "test", false),
                new CommitItem("1234", "testauthor", DateTime.Now, "this should not pass test", "test", false),
                new CommitItem("1234", "testauthor", DateTime.Now, "this should pass test1", "test", false)
            };
            List<ICommitItem> FilteredCommitList = new List<ICommitItem>();
            //Ausführen des zu testenden Befehls
            FilteredCommitList = filter.ApplyIncludeFilterList(UnfilteredCommitList);
            //Sicherstellen, dass der Befehl korrekt ausgeführt wurde
            Assert.AreNotEqual(UnfilteredCommitList, FilteredCommitList);
            Assert.AreEqual(FilteredCommitList.Count, 1);
            Assert.AreEqual(FilteredCommitList[0].CommitMessage, "this should pass test1");
        }

        [TestMethod()]
        public void addIncludeFilterToListTest()
        {
            // Initialisierung der benötigten Klassen
            Filter filter = new Filter();
            bool caught = false;
            bool caught2 = false;
            ObservableCollection<string> ListWithoutDuplicateEntries = new ObservableCollection<string>
            {
                "Test1",
                "Test2"
            };
            ObservableCollection<string> ListWithDuplicateEntries = new ObservableCollection<string>
            {
                "Test1",
                "Test1",
                "Test2",
                "Test2"
            };
            // Ausführen der zu testenden Methode mit zwischengelagerter Überprüfung auf korrektheit
            caught = filter.addIncludeFilterToList("Test1");
            caught2 = filter.addIncludeFilterToList("Test2");
            Assert.IsTrue(caught);
            Assert.IsTrue(caught2);
            caught = filter.addIncludeFilterToList("Test1");
            caught2 = filter.addIncludeFilterToList("Test2");
            Assert.IsFalse(caught);
            Assert.IsFalse(caught2);
            // Finale Überprüfung auf Korrektheit
            Assert.AreNotEqual(filter.IncludeFilterList.Count, ListWithDuplicateEntries.Count);
            Assert.AreEqual(filter.IncludeFilterList.Count, ListWithoutDuplicateEntries.Count);
        }

        [TestMethod()]
        public void deleteIncludeFilterFromListTest()
        {
            // Initialisiern der zu testenden Strukturen
            Filter filter = new Filter();
            filter.addIncludeFilterToList("Test1"); 
            filter.addIncludeFilterToList("Test2"); 
            filter.addIncludeFilterToList("Test3");
            bool deleted = false;
            ObservableCollection<string> ListWithAllTestValues = new ObservableCollection<string>() { "Test1", "Test2", "Test3" };
            ObservableCollection<string> ListWithExpectedTestValues = new ObservableCollection<string>() { "Test1", "Test3" };
            // ausführen der zu testenden Methode und zwischengelagerte Überpüfung
            deleted = filter.deleteIncludeFilterFromList("Test2");
            Assert.IsTrue(deleted);
            deleted = filter.deleteIncludeFilterFromList("Test2");
            Assert.IsFalse(deleted);

            Assert.AreEqual(ListWithExpectedTestValues.Count,filter.IncludeFilterList.Count);
            Assert.AreNotEqual(ListWithAllTestValues.Count, filter.IncludeFilterList.Count);
        }

        [TestMethod()]
        public void addExcludeFilterToListTest()
        {
            // Initialisierung der benötigten Klassen
            Filter filter = new Filter();
            bool caught = false;
            bool caught2 = false;
            ObservableCollection<string> ListWithoutDuplicateEntries = new ObservableCollection<string>
            {
                "Test1",
                "Test2"
            };
            ObservableCollection<string> ListWithDuplicateEntries = new ObservableCollection<string>
            {
                "Test1",
                "Test1",
                "Test2",
                "Test2"
            };
            // Ausführen der zu testenden Methode mit zwischengelagerter Überprüfung auf korrektheit
            caught = filter.addExcludeFilterToList("Test1");
            caught2 = filter.addExcludeFilterToList("Test2");
            Assert.IsTrue(caught);
            Assert.IsTrue(caught2);
            caught = filter.addExcludeFilterToList("Test1");
            caught2 = filter.addExcludeFilterToList("Test2");
            Assert.IsFalse(caught);
            Assert.IsFalse(caught2);
            // Finale Überprüfung auf Korrektheit
            Assert.AreNotEqual(filter.ExcludeFilterList.Count, ListWithDuplicateEntries.Count);
            Assert.AreEqual(filter.ExcludeFilterList.Count, ListWithoutDuplicateEntries.Count);
        }

        [TestMethod()]
        public void deleteExcludeFilterFromListTest()
        {
            // Initialisiern der zu testenden Strukturen
            Filter filter = new Filter();
            filter.addExcludeFilterToList("Test1"); 
            filter.addExcludeFilterToList("Test2"); 
            filter.addExcludeFilterToList("Test3");
            bool deleted = false;
            ObservableCollection<string> ListWithAllTestValues = new ObservableCollection<string>() { "Test1", "Test2", "Test3" };
            ObservableCollection<string> ListWithExpectedTestValues = new ObservableCollection<string>() { "Test1", "Test3" };
            // ausführen der zu testenden Methode und zwischengelagerte Überpüfung
            deleted = filter.deleteExcludeFilterFromList("Test2");
            Assert.IsTrue(deleted);
            deleted = filter.deleteExcludeFilterFromList("Test2");
            Assert.IsFalse(deleted);

            Assert.AreEqual(ListWithExpectedTestValues.Count,filter.ExcludeFilterList.Count);
            Assert.AreNotEqual(ListWithAllTestValues.Count, filter.ExcludeFilterList.Count);
        }
    }
}