﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using gitlogtool.logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using miniLib;

namespace gitlogtool.logic.Tests
{
    [TestClass()]
    public class SubstitutionListTests
    {
        [TestMethod()]
        public void SubstituteFileNamesTest()
        {
            // assert
            SubstitutionList subs = new SubstitutionList();
            List<ICommitItem> inputlist = new List<ICommitItem>();
            SubstitutionItem newItem = new SubstitutionItem("trigger", "TESTVALUE");
            subs.Add(newItem);
            CommitItem item1 = (new CommitItem("1234", "TestAuthor", DateTime.Now, "This is a test Message", @"\test1\test1\TestFile.cs", false));
            CommitItem item2 = (new CommitItem("1235", "TestAuthor", DateTime.Now, "This is a trigger test Message", @"\test2\test2\TestFile.cs", false));
            CommitItem item3 = (new CommitItem("1236", "TestAuthor", DateTime.Now, "This is a trigger test Message", @"\test3\test3\TesttriggerFile.cs", false));
            inputlist.AddRange(new List<ICommitItem>{ item1,item2,item3});
            // act
            var outpulistFilenames = subs.SubstituteFileNames(inputlist);
            // assert
            Assert.AreEqual(outpulistFilenames[0], item1);
            Assert.AreEqual(outpulistFilenames[1].CommitFilePath, item2.CommitFilePath);
            Assert.AreEqual(outpulistFilenames[2].CommitFileDisplayName, item3.CommitFileDisplayName);
            Assert.AreEqual(outpulistFilenames[2].CommitFileFullPath, @"\test3\test3\TesttriggerFile.cs");
            Assert.AreEqual(outpulistFilenames[2].CommitFileDisplayName, @"TESTVALUE");
        }

        [TestMethod()]
        public void SubstituteMessagesTest()
        {
            // assert
            SubstitutionList subs = new SubstitutionList();
            List<ICommitItem> inputlist = new List<ICommitItem>();
            SubstitutionItem newItem = new SubstitutionItem("trigger", "TESTVALUE");
            subs.Add(newItem);
            CommitItem item1 = (new CommitItem("1234", "TestAuthor", DateTime.Now, "This is a test Message", @"\test1\test1\TestFile.cs", false));
            CommitItem item2 = (new CommitItem("1235", "TestAuthor", DateTime.Now, "This is a trigger test Message", @"\test2\test2\TestFile.cs", false));
            CommitItem item3 = (new CommitItem("1236", "TestAuthor", DateTime.Now, "This is a triggertest Mes-trigger-sage", @"\test3\test3\TesttriggerFile.cs", false));
            inputlist.AddRange(new List<ICommitItem>{ item1,item2,item3});
            // act
            var outputMessages = subs.SubstituteMessages(inputlist);
            // assert
            Assert.AreEqual(outputMessages[0].CommitMessage, "This is a test Message");
            Assert.AreEqual(outputMessages[1].CommitMessage, "This is a TESTVALUE test Message");
            Assert.AreEqual(outputMessages[2].CommitMessage, "This is a TESTVALUEtest Mes-TESTVALUE-sage");
            Assert.AreEqual(outputMessages[2].CommitFileDisplayName, @"TesttriggerFile.cs");
            Assert.AreEqual(outputMessages[2].CommitFileFullPath, @"\test3\test3\TesttriggerFile.cs");
        }
    }
}