﻿using miniLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace gitlogtool.logic
{
    public static class CSVExport
    {
        static string sep = ";";
        static StreamWriter log;
        public static bool export(IEnumerable<ICommitItem> inputList, string branchname, string workingDir, bool hideDocCommits = false)
        {
            bool success = false;
            try
            {
                prepareCSV(branchname, workingDir);

                foreach (var item in inputList)
                {
                    if (String.IsNullOrEmpty(item.CommitSHA))
                        continue;
                    if (!item.validItem)
                        continue;
                    if (hideDocCommits && item.CommitDocumented.HasValue && item.CommitDocumented.Value)
                        continue;

                    logToFile(new string[]
                    {
                        Path.Combine(item.CommitFilePath,
                        item.CommitFileDisplayName),
                        item.CommitDate.ToString(),
                        item.CommitMessage,
                        item.CommitSHA,
                        item.CommitDocumented.ToString()
                    });
                }
                log.Flush();
                log.Close();
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fehler beim Export");
                Console.WriteLine(ex.Message);
                success = false;
            }
            return success;
        }
        private static void prepareCSV(string branch, string workingDir)
        {
            string date = DateTime.Now.ToString().Replace(':', '_');
            branch = branch.Replace('/', '_');
            branch = branch.Replace(':', '_');
            string filename = Path.Combine(workingDir, branch + "_changelog.csv");
            log = new StreamWriter(filename, false, Encoding.Default);
            log.WriteLine("Filename;Date;Message;CommitSHA;Documented");
        }
        private static void logToFile(string[] input)
        {
            string temp = "";
            foreach (var item in input)
            {
                temp += item;
                temp += sep;
            }
            log.WriteLine(temp);
        }
    }
}
