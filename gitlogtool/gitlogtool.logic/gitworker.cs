﻿using miniLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Markup;

namespace gitlogtool.logic
{
    public class gitworker : INotifyPropertyChanged
    {
        #region Member
        private SuspendableObservableCollection<CommitItem> items;

        private string branchName;
        private string CommitID;
        private string workingPath = "";
        private string commitAuthor;
        private string commitMessage;
        private string committedFile;
        private string committedFilePath;
        private DateTime committDate;

        private DateTime startDate;
        private DateTime endDate;

        private string workerstate = "";
        private object lockobj = new object();
        #endregion

        #region Interface
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Accessoren
        public string WorkingPath
        {
            get => workingPath;
            set
            {
                if (workingPath == value)
                    return;
                workingPath = value;
                OnPropertyChanged("WorkingPath");
            }
        }
        public DateTime StartDate
        {
            get => startDate;
            set
            {
                if (startDate == value)
                    return;
                startDate = value;
                OnPropertyChanged("StartDate");
            }
        }
        public DateTime EndDate
        {
            get => endDate;
            set
            {
                if (endDate == value)
                    return;
                endDate = value;
                OnPropertyChanged("EndDate");
            }
        }
        public string StartDateString
        {
            get => StartDate.ToString("yyyy-MM-dd");
        }
        public string EndDateString
        {
            get => EndDate.ToString("yyyy-MM-dd");
        }
        public SuspendableObservableCollection<CommitItem> Items
        {
            get => items;
            set
            {
                if (items == value)
                    return;
                items = value;
                OnPropertyChanged("Items");
            }
        }
        [DependsOn(nameof(Workerstate))]
        public bool WorkerBusy
        {
            get
            {
                return (String.Equals(Workerstate.ToLower(), "busy"));
            }
        }
        public string Workerstate
        {
            get => workerstate;
            set
            {
                if (workerstate == value)
                    return;
                workerstate = value;
                OnPropertyChanged("Workerstate");
            }
        }

        public string BranchName
        {
            get => branchName;
            set
            {
                if (branchName == value)
                    return;
                branchName = value;
                OnPropertyChanged("BranchName");
            }
        }
        #endregion

        #region Konstruktor
        public gitworker()
        {
            items = new SuspendableObservableCollection<CommitItem>();
        }

        public gitworker(DateTime _startDate, DateTime? _endDate = null, string _workingPath = "") : this()
        {
            if (_endDate.HasValue)
                if (_startDate > _endDate)
                    throw new ArgumentException("Start Date is bigger than EndDate. Invalid Time Frame");
            if (_startDate > DateTime.Now)
                throw new ArgumentException("Start Date is in the future. Invalid Time Frame");

            this.StartDate = _startDate;

            if (_endDate.HasValue)
                this.EndDate = _endDate.Value;
            else
                this.EndDate = DateTime.Now;

            if (!String.IsNullOrEmpty(_workingPath))
                this.WorkingPath = _workingPath;
            else
                this.WorkingPath = Directory.GetCurrentDirectory();
        }
        #endregion

        #region Main Functions
        /// <summary>
        /// Requires a git Repo Path to be set, as well as a Time Span. Fetches the git log of the specified Date Range 
        /// and transforms it into a list of changed files with their respective Commit SHA, Author and Date.
        /// </summary>
        /// <returns>A List of CommitItems, ordered by CommitItem.FilePath</returns>
        public List<ICommitItem> doWork()
        {
            var outList = new List<ICommitItem>();
            if (!canRun())
                return outList;
            if (!checkDate())
                return outList;
            if (this.WorkerBusy)
                return outList;

            Workerstate = "busy";
            clearPresents();
            string logstring = acquireGitLog();

            if (String.IsNullOrEmpty(logstring))
                return null;

            outList = parse(logstring);

            Workerstate = "";

            return outList;
        }

        private bool checkDate()
        {
            if (this.StartDate > this.EndDate)
                throw new ArgumentException("Start Date is bigger than EndDate. Invalid Time Frame");
            if (this.StartDate > DateTime.Now)
                throw new ArgumentException("Start Date is in the future. Invalid Time Frame");
            return true;
        }

        /// <summary>
        /// Decides wether the gitworker will be able to function with the specified parameters.
        /// </summary>
        /// <returns>False if the RepoPath is non-existent, not a git Repo or git itself is not installed. Else True.</returns>
        private bool canRun()
        {
            bool run = true;
            if (!Directory.Exists(WorkingPath))
            {
                throw new DirectoryNotFoundException("Directory not found. Please choose a valid path.");
            }
            if (!util.checkForGit())
            {
                throw new FileNotFoundException("git not present on System. Please install git.");
            }
            if (!util.isGitRepo(WorkingPath))
            {
                throw new DirectoryNotFoundException("Not a valid git Repository.");
            }
            if (this.StartDate > this.EndDate)
            {
                throw new ArgumentOutOfRangeException("StartDate is bigger than EndDate. Invalid TimeFrame");
            }
            return run;
        }
        /// <summary>
        /// Resets the global variables to empty.
        /// </summary>
        private void clearPresents()
        {
            Items.Clear();
            commitAuthor = "";
            CommitID = "";
            committDate = DateTime.MinValue;
            commitMessage = "";
            committedFile = "";
        }
        /// <summary>
        /// Requests git parameters to be set up and runs them against git. 
        /// </summary>
        /// <returns>The output of git that is not yet usable.</returns>
        private string acquireGitLog()
        {

            string revparseargs = (" rev-parse --abbrev-ref HEAD");
            string logargs = " log --after=\"" + StartDateString + "\" ";
            if (!String.IsNullOrEmpty(EndDateString))
                logargs += ("--before=\"" + EndDateString + "\" ");
            logargs += ("--name-only --no-merges --format=\"commit$%H|author$%an|date$%at|%n|message$%s|\"");

            string outString = "branch$";

            outString += util.doGitCommand(revparseargs, WorkingPath);
            outString += util.doGitCommand(logargs, WorkingPath);

            return outString;
        }
        /// <summary>
        /// Parses the acquired git log into an ordered List of CommitItems
        /// </summary>
        /// <param name="logString">The Output of acquireGiLog()</param>
        /// <returns>An ordered List of CommitItem, or empty List if there is nothing to parse.</returns>
        private List<ICommitItem> parse(string logString)
        {
            List<ICommitItem> outList = new List<ICommitItem>();
            string[] chunks;
            var lines = logString.Replace("\n", "§").Split('§');

            foreach (var line in lines)
            {
                chunks = line.Split('|');
                if (chunks[0] != "" && chunks.Count() == 1 && !chunks[0].ToLower().StartsWith("branch$"))
                {

                    committedFile = util.convertUmlaut(chunks[0]);
                    committedFile = committedFile.Replace("\"", "");
                    committedFilePath = Path.GetDirectoryName(committedFile);
                    committedFile = Path.GetFileName(committedFile);

                    // After the block of metadata about the Commit itself a list of files follows
                    // We can use the already set information again until we hit the next line of 
                    // metadata. 
                    CommitItem tempItem = new CommitItem()
                    {
                        CommitAuthor = this.commitAuthor,
                        CommitDate = this.committDate,
                        CommitMessage = this.commitMessage,
                        CommitSHA = this.CommitID,
                        CommitFileName = this.committedFile,
                        CommitFileDisplayName = this.committedFile,
                        CommitFilePath = this.committedFilePath,
                        CommitDocumented = false
                    };
                    outList.Add(tempItem);
                }
                else
                {
                    if (chunks.Count() < 1) //if the chunk contains less than 1 items it's the filename or a blank line
                        continue;

                    foreach (var chunk in chunks)
                    {
                        // the helper functions use side effects to set the git commit metadata
                        // not the cleanest architecture in the world but it get's the job done.
                        if (chunk != "" && chunk != null)
                        {
                            if (isChunkCommitID(chunk))
                                continue;
                            if (isAuthor(chunk))
                                continue;
                            if (isDate(chunk))
                                continue;
                            if (isCommitMessage(chunk))
                                continue;
                            if (isBranch(chunk))
                                continue;
                        }
                        else continue;
                    }
                }
            }
            return outList.OrderBy(x => x.CommitFilePath).ToList();
        }
        #endregion

        #region Helper Functions
        /// <summary>
        /// Checks wether the input string is a Commit SHA chunk and if so sets it in the class Variable
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns>True if the string is a Commit SHA chunk</returns>
        private bool isChunkCommitID(string chunk)
        {
            if (!String.IsNullOrWhiteSpace(chunk) && chunk.StartsWith("commit"))
            { //if commit ID is found set the current commit so everything that follows will be properly set
                CommitID = chunk.Split('$')[1];
                return true;
            }
            else { return false; }
        }
        /// <summary>
        /// Checks wether the input string is a Commit Message chunk and if so sets it in the class Variable
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns>True if the string is a Commit Message chunk</returns>
        private bool isCommitMessage(string chunk)
        {
            if (!String.IsNullOrWhiteSpace(chunk) && chunk.ToLower().StartsWith("message"))
            {
                commitMessage = util.convertUmlaut(chunk.Split('$')[1].Replace(";", "\\;"));
                return true;
            }
            else { return false; }
        }
        /// <summary>
        /// Check wether the input string is a Commit Date and, if it can parse it, sets it in the class Variable
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns>True if the string is a Commit Date chunk.</returns>
        private bool isDate(string chunk)
        {
            if (!String.IsNullOrWhiteSpace(chunk) && chunk.ToLower().StartsWith("date"))
            {
                if (chunk.Split('$')[1] != "")
                {
                    committDate = DateTimeOffset.FromUnixTimeSeconds(int.Parse(chunk.Split('$')[1].ToString())).UtcDateTime;
                    return true;
                }
                else { return false; }
            }
            else { return false; }
        }
        /// <summary>
        /// Checks wether the input string is Commit Author chunk and, if so, sets it in the class Variable
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns>True if the input string is a Commit Author chunk.</returns>
        private bool isAuthor(string chunk)
        {
            if (!String.IsNullOrWhiteSpace(chunk) && chunk.ToLower().StartsWith("author"))
            {
                if (chunk.Split('$')[1].ToLower().ToString() == "")
                {
                    commitAuthor = "unknown";
                    return true;
                }
                else
                {
                    commitAuthor = util.convertUmlaut(chunk.Split('$')[1].ToString());
                    return true;
                }
            }
            else { return false; }
        }
        private bool isBranch(string chunk)
        {
            if (!String.IsNullOrWhiteSpace(chunk) && chunk.ToLower().StartsWith("branch"))
            {
                if (chunk.Split('$')[1].ToLower().ToString() == "")
                {
                    BranchName = "unknown";
                    return true;
                }
                else
                {
                    BranchName = util.convertUmlaut(chunk.Split('$')[1].ToString());
                    return true;
                }
            }
            else { return false; }
        }
        #endregion
    }
}