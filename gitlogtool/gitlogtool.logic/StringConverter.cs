﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace gitlogtool.logic
{
    public class GitUserNameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return String.Format("{0}/{1}", values[0], values[1]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class TitleNameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return String.Format("{0} {1} {2}", values[0], ((!String.IsNullOrWhiteSpace(values[1].ToString())) ? "-" : ""), values[1]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return value.ToString().Split('-');
        }
    }
    public class DocVersionAndDate : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime comDate = (DateTime)values[0];
            string datString = "";
            if (comDate > new DateTime(0001, 1, 1))
                datString = comDate.ToString("d");

            if (values.Length == 3)
                return String.Format("{0} {1} {2}", datString, values[1], values[2]);
            else
                return String.Format("{0} {1}", datString, values[1]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
