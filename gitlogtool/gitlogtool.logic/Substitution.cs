﻿using GalaSoft.MvvmLight.Threading;
using miniLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace gitlogtool.logic
{
    public class SubstitutionList : INotifyPropertyChanged
    {
        private ObservableCollection<SubstitutionItem> list;
        private string workingPath;
        private string documentFilename = Constants.SUBSTITUTIONLIST;

        #region Properties
        public ObservableCollection<SubstitutionItem> SubList
        {
            get => list;
            set
            {
                list = value;
                OnPropertyChanged("SubList");
            }
        }
        public string WorkingPath
        {
            get => workingPath;
            set
            {
                if (value == workingPath)
                    return;
                workingPath = value;
                OnPropertyChanged("FilterList");
            }
        }
        public SubstitutionList()
        {
            SubList = new ObservableCollection<SubstitutionItem>();
        }
        #endregion

        #region Interface 
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Do Substitution
        public List<ICommitItem> SubstituteMessages(List<ICommitItem> items)
        {
            foreach (var item in items)
            {
                foreach (SubstitutionItem sub in SubList)
                {
                    item.CommitMessage = item.CommitMessage.Replace(sub.InputVal, sub.OutputVal);
                }
            }
            return items;
        }
        public List<ICommitItem> SubstituteFileNames(List<ICommitItem> items)
        {
            foreach (var item in items)
            {
                foreach (SubstitutionItem sub in SubList)
                {
                    if (item.CommitFileName.Contains(sub.InputVal))
                        item.CommitFileDisplayName = sub.OutputVal;
                }
            }
            return items;
        }
        #endregion

        #region Methods
        public bool? SaveList(string workingdir)
        {
            if (SubList.Count <= 0)
                return false;
            if (String.IsNullOrEmpty(workingdir))
                return false;
            if (!Directory.Exists(workingdir))
                return false;

            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(Path.Combine(workingdir, documentFilename)))
                {
                    foreach (var item in SubList)
                    {
                        file.WriteLine(item.InputVal + '|' + item.OutputVal);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public void ReadSubstitutionsFromDisk(string workingdir)
        {
            Task.Run(() =>
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    SubList.Clear();
                    if (File.Exists(Path.Combine(workingdir, documentFilename)))
                    {
                        var lines = File.ReadAllLines(Path.Combine(workingdir, documentFilename));
                        foreach (var item in lines)
                        {
                            var chunks = item.Split('|');
                            SubList.Add(new SubstitutionItem(chunks[0], chunks[1]));
                        }
                    }
                });
            });
        }
        public void Remove(SubstitutionItem selectedSubstitutionItem)
        {
            if (SubList.Contains(selectedSubstitutionItem))
                SubList.Remove(selectedSubstitutionItem);
        }
        public void Add(SubstitutionItem selectedSubstitutionItem)
        {
            if (String.IsNullOrEmpty(selectedSubstitutionItem.InputVal))
                return;
            if (String.Equals(selectedSubstitutionItem.InputVal, selectedSubstitutionItem.OutputVal))
                return;

            bool ok = true;
            foreach (var item in SubList)
            {
                if (!ok)
                    break;
                if (item.InputVal == selectedSubstitutionItem.InputVal)
                    ok = false;
            }
            if (ok)
                this.SubList.Add(selectedSubstitutionItem);
        }
        #endregion
    }
    public class SubstitutionItem : INotifyPropertyChanged
    {
        private string _inputVal = "";
        private string _outputVal = "";

        public SubstitutionItem(string inputVal, string outputVal)
        {
            _inputVal = inputVal;
            _outputVal = outputVal;
        }

        public string InputVal
        {
            get => _inputVal;
            set
            {
                _inputVal = value;
                OnPropertyChanged("InputVal");

            }
        }
        public string OutputVal
        {
            get => _outputVal;
            set
            {
                _outputVal = value;
                OnPropertyChanged("OutputVal");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
