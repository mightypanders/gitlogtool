﻿using miniLib;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gitlogtool.logic
{
    public static class TreeHelper
    {
        public static IEnumerable<ICommitItem> buildBasicTreeList(List<ICommitItem> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].Key = list[i].CommitFileFullPath;
                if (list[i].CommitFilePath == "")
                {
                    list[i].Parent = "root";
                    continue;
                }
                else list[i].Parent = list[i].CommitFilePath;

                if (!list.Any(item => item.CommitFileFullPath == list[i].CommitFilePath))
                {
                    var tempItem = (new CommitItem("xxxxxxxx", "xxx", DateTime.Now, "xxx", list[i].CommitFilePath, null) { });
                    tempItem.Key = tempItem.CommitFileFullPath;
                    tempItem.Parent = tempItem.CommitFilePath;
                    if (tempItem.Parent == "")
                        tempItem.Parent = "root";
                    list.Add(tempItem);
                }
            }
            list = list.OrderBy(x => x.CommitFileFullPath).ToList();
            //list[0].CommitFilePath = "root";
            return list;
        }

        private static void setKeys(IEnumerable<ICommitItem> list)
        {
            foreach (var item in list)
            {
                item.Key = item.CommitFileFullPath + "||" + item.ShortSHA;
                item.Parent = item.CommitFileFullPath;
            }
        }

        public static IEnumerable<ICommitItem> BasicTree(List<ICommitItem> list)
        {
            setKeys(list);

            for (int i = 0; i < list.Count; i++)
            {
                bool found = false;
                if (String.IsNullOrEmpty(list[i].Parent))
                    continue;
                for (int j = list.Count - 1; j >= 0; j--)
                {
                    if (list[i].Parent == list[j].Key)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    var tempItem = new CommitItem("xxxxxxxx", "xxx", DateTime.MinValue, "xxx", list[i].Parent, null);

                    list.Add(tempItem);
                    //Console.WriteLine("added: " + tempItem.ToString());

                }

            }
            list = list.OrderBy(y => y.Key).ToList();
            //list.Where(x => String.IsNullOrEmpty(x.Parent)).Select(x => { x.Parent = "root"; return true; }).ToList();
            //list[0].Parent = "root";
            return list;
        }
    }
}
