﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace gitlogtool.logic
{
    public class gitUser : INotifyPropertyChanged
    {
        private string username = "";
        private string fullname = "";

        public gitUser()
        {
            Username= System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        }
        public gitUser(string username, string fullname)
        {
            this.username = username;
            this.fullname = fullname;
        }

        public string Username
        {
            get => username;
            set
            {
                if (username == value)
                    return;
                username = value;
                OnPropertyChanged("Username");
            }
        }
        public string Fullname
        {
            get => fullname;
            set
            {
                if (fullname == value)
                    return;
                fullname = value;
                OnPropertyChanged("Fullname");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public override string ToString()
        {
            return this.Username + "/" + this.Fullname.Replace('|','\\');
        }
    }
}
