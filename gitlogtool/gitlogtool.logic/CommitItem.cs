﻿using miniLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Markup;

namespace gitlogtool.logic
{
    public class CommitItem : INotifyPropertyChanged, IDataErrorInfo, IDisposable, ICommitItem
    {
        private string _commitSHA;
        private string _commitAuthor;
        private DateTime _commitDate;
        private string _commitMessage;
        private string _committedFileName;
        private string _committedFilePath;
        private bool? _commitDocumented;
        private string _commitFileDisplayName;

        private string _documentingUser;
        private string _documentedVersion;
        private DateTime _documentedDate;
        private string _parent;
        private string _key;

        #region Constructor

        public CommitItem()
        {

        }
        public CommitItem(string sha, string author, DateTime date, string message, string filepath, bool? documented)
        {
            this.CommitSHA = sha;
            this.CommitAuthor = author;
            this.CommitDate = date;
            this.CommitMessage = message;
            this.CommitFileName = Path.GetFileName(filepath);
            this.CommitFileDisplayName = this.CommitFileName;
            this.CommitFilePath = (String.IsNullOrEmpty(filepath)) ? "" : Path.GetDirectoryName(filepath);
            this.CommitDocumented = documented;
            this.Key = this.CommitFileFullPath;
            this.Parent = this.CommitFilePath;
        }
        #endregion
        #region Interface
        private Dictionary<string, string> Errors { get; } = new Dictionary<string, string>();
        private void CollectErrors()
        {
            Errors.Clear();
            if (string.IsNullOrEmpty(CommitSHA))
            {
                Errors.Add(nameof(CommitSHA), "SHA may not bet empty");
            }
            if (CommitSHA.Length < 7)
            {
                Errors.Add(nameof(CommitSHA), "SHA may not be shorter than 7 chars");
            }
        }
        public string this[string columnName]
        {
            get
            {
                CollectErrors();
                return Errors.ContainsKey(columnName) ? Errors[columnName] : string.Empty;
            }
        }
        public string Error => "";
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region Methods

        public void Clear()
        {
            this.CommitAuthor = "";
            this.CommitDate = DateTime.Now;
            this.CommitMessage = "";
            this.CommitSHA = "";
            this.CommitFilePath = "";
        }

        public void Dispose()
        {
            this.Clear();
        }

        public override string ToString()
        {
            return this.Parent + "|" + this.Key;
        }
        public string ToString(bool docString)
        {
            return this.CommitSHA + "|" + this.CommitFileFullPath;
        }

        #endregion

        #region Props
        public string CommitSHA
        {
            get => _commitSHA;
            set
            {
                if (value == _commitSHA)
                    return;
                _commitSHA = value;
                OnPropertyChanged("");
            }
        }
        [DependsOn(nameof(CommitSHA))]
        public string ShortSHA => CommitSHA.SafeSubstring(0, 7);
        [DependsOn(nameof(CommitFileName))]
        [DependsOn(nameof(CommitFilePath))]
        public string CommitFileFullPath
        {
            get
            {
                return Path.Combine(CommitFilePath, CommitFileName);
            }
        }
        public string CommitAuthor
        {
            get => _commitAuthor;
            set
            {
                if (value == _commitAuthor)
                    return;
                _commitAuthor = value;
                OnPropertyChanged("");
            }
        }
        public DateTime CommitDate
        {
            get => _commitDate;
            set
            {
                if (value == _commitDate)
                    return;
                _commitDate = value;
                OnPropertyChanged("");
            }
        }
        public string CommitMessage
        {
            get => _commitMessage;
            set
            {
                if (value == _commitMessage)
                    return;
                _commitMessage = value;
                OnPropertyChanged("");
            }
        }
        public string CommitFilePath
        {
            get => _committedFilePath;
            set
            {
                if (value == _committedFilePath)
                    return;
                _committedFilePath = value;
                OnPropertyChanged("");
            }
        }
        public string CommitFileName
        {
            get => _committedFileName;
            set
            {
                if (_committedFileName == value)
                    return;
                _committedFileName = value;
                OnPropertyChanged("CommitFileName");
            }
        }
        public string CommitFileDisplayName
        {
            get => _commitFileDisplayName;
            set
            {
                if (_commitFileDisplayName == value)
                    return;
                _commitFileDisplayName = value;
                OnPropertyChanged("CommitFileDisplayName");
            }
        }
        public bool? CommitDocumented
        {
            get => _commitDocumented;
            set
            {
                if (value == _commitDocumented)
                    return;
                _commitDocumented = value;
                OnPropertyChanged("");
            }
        }
        public string DocumentingUser
        {
            get => _documentingUser;
            set
            {
                if (value == _documentingUser)
                    return;
                _documentingUser = value;
                OnPropertyChanged("");
            }
        }
        public string DocumentedVersion
        {
            get => _documentedVersion;
            set
            {
                if (value == _documentedVersion)
                    return;
                _documentedVersion = value;
                OnPropertyChanged("");
            }
        }
        public DateTime DocumentedDate
        {
            get => _documentedDate;
            set
            {
                if (value == _documentedDate)
                    return;
                _documentedDate = value;
                OnPropertyChanged("");
            }
        }
        [DependsOn(nameof(CommitFilePath))]
        public string ParentDirectory
        {
            get => (String.IsNullOrWhiteSpace(CommitFilePath)) ? "" : Path.GetDirectoryName(CommitFilePath);
        }
        public string Parent
        {
            get => _parent;
            set
            {
                if (value == _parent)
                    return;
                _parent = value;
                OnPropertyChanged("Parent");
            }
        }
        [DependsOn(nameof(CommitSHA))]
        public bool validItem
        {
            get => !String.Equals(this.CommitSHA, "xxxxxxxx");
        }
        public string Key
        {
            get => _key;
            set
            {
                if (value == _key)
                    return;
                _key = value;
                OnPropertyChanged("Key");
            }
        }
        #endregion
    }
}