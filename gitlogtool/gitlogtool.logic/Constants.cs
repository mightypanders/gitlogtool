﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gitlogtool.logic
{
    public static class Constants
    {
        public const string DOCUMENTEDCOMMITS = ".documentedCommits";
        public const string EXCLUDEFILTERLIST = ".savedExcludeFilters";
        public const string INCLUDEFILTERLIST = ".savedIncludeFilters";
        public const string SUBSTITUTIONLIST = ".savedSubstitutions";

        public static IEnumerable<string> FILENAMELIST = new List<string>() { DOCUMENTEDCOMMITS, EXCLUDEFILTERLIST, INCLUDEFILTERLIST, SUBSTITUTIONLIST };
    }
}
