using DevExpress.Mvvm;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Threading;
using gitlogtool.logic.Properties;
using miniLib;
using MvvmDialogs.FrameworkDialogs.FolderBrowser;
using MvvmDialogs.FrameworkDialogs.SaveFile;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace gitlogtool.logic.ViewModel
{
    public class MainViewModel : GalaSoft.MvvmLight.ViewModelBase, ISupportServices
    {
        #region Member
        public RelayCommand DoWorkCommand { get; private set; }
        public RelayCommand SaveCommand { get; private set; }
        public RelayCommand BrowseCommand { get; private set; }
        public RelayCommand SaveExcludeFilterListCommand { get; private set; }
        public RelayCommand SaveIncludeFilterListCommand { get; private set; }
        public RelayCommand SaveSubListCommand { get; private set; }
        public RelayCommand SaveSubCommand { get; private set; }
        public RelayCommand<string> ExportCommand { get; private set; }
        public RelayCommand TestError { get; private set; }
        public RelayCommand WindowClosing { get; private set; }
        public RelayCommand DelSubCommand { get; private set; }
        public RelayCommand<string> EvalDirCommand { get; private set; }
        public RelayCommand<string> SaveExcludeFilterCommand { get; private set; }
        public RelayCommand<string> DeleteExcludeFilterCommand { get; private set; }
        public RelayCommand<string> SaveIncludeFilterCommand { get; private set; }
        public RelayCommand<string> DeleteIncludeFilterCommand { get; private set; }

        private object _lockCommits = new object();
        private object _lockSubs = new object();
        private object _lockTree = new object();
        private IServiceContainer serviceContainer;

        //private SuspendableObservableCollection<TreeNode> _nodeList;
        private SuspendableObservableCollection<ICommitItem> _commitcollection;
        private DateTime endDate = DateTime.Today;
        private DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        protected virtual IExportService ExportService { get { return ServiceContainer.GetService<IExportService>(); } }


        private SolidColorBrush bgCol = new SolidColorBrush(Colors.White);
        private string repopath = "";
        private string versionTag = "";

        private gitUser user;
        private CommitItem item;
        private DocumentedCommits docCommits;

        private SubstitutionItem _selectedSubItem;
        private string _excludefilterToAdd = "";
        private string _includeFilterToAdd = "";
        private Filter filter;
        private SubstitutionList substitution;
        private bool _useSubs = true;
        private bool _useExclude = true;
        private bool _useInclude = true;
        private bool _hideDocCommits = true;
        private bool _hideToolFiles = true;
        private long _commitLoadTime;
        private readonly MvvmDialogs.IDialogService dialogService;
        private string confirmation;
        private int _selectedTabIndex = 1;
        private SubstitutionItem _focusedSubItem;
        #endregion

        #region Konstruktor
        public MainViewModel(MvvmDialogs.IDialogService idialog)
        {
            dialogService = idialog;
            item = new CommitItem();
            FilterClass = new Filter();
            SubstitutionHandling = new SubstitutionList();
            commitcollection = new SuspendableObservableCollection<ICommitItem>();
            SelectedSubstitutionItem = new SubstitutionItem("", "");
            TreeItems = new SuspendableObservableCollection<TreeItem>();

            initButtonCommands();
            if (!IsInDesignMode)
                initGitWorker();
            user = new gitUser();
            SetRepoPath();
            SetVersionNumber();
            evaluateWorkingPath(RepoPath);
#if (DEBUG)
            //FilterClass.FilterList.Add("NDR");
            //RepoPath = @"C:\Users\markus\Documents\schuldokumente";
            //RepoPath = @"C:\Users\mardie\Development\VS2015\Docusuite";
#endif
        }

        private void SetVersionNumber()
        {
            if (!String.IsNullOrEmpty(Settings.Default.VersionTag))
                this.VersionTag = Settings.Default.VersionTag;
            else
                this.versionTag = "";
        }

        private void SetRepoPath()
        {
            if (!String.IsNullOrEmpty(Settings.Default.RepoPath))
                RepoPath = Settings.Default.RepoPath;
            else
                RepoPath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        }
        #endregion

        #region Helper
        private void initGitWorker()
        {
            Worker = new gitworker();
            initCollectionSync();
        }
        private void initCollectionSync()
        {
            BindingOperations.EnableCollectionSynchronization(commitcollection, _lockCommits);
            BindingOperations.EnableCollectionSynchronization(TreeItems, _lockTree);
            BindingOperations.EnableCollectionSynchronization(SubstitutionHandling.SubList, _lockSubs);
        }
        private void WorkerDoWork()
        {
            this.commitcollection = new SuspendableObservableCollection<ICommitItem>();
            this.TreeItems = new SuspendableObservableCollection<TreeItem>();
            initCollectionSync();
            Task.Run(() =>
            {
                Stopwatch stopper = new Stopwatch();
                stopper.Start();
                docCommits = new DocumentedCommits(this.RepoPath);
                Worker.StartDate = this.StartDate;
                Worker.EndDate = this.EndDate;
                Worker.WorkingPath = this.RepoPath;
                var tmpCol = new List<ICommitItem>();
                try
                {
                    tmpCol = Worker.doWork();


                    tmpCol = performModifyOperations(tmpCol);
                    tmpCol = TreeHelper.BasicTree(tmpCol).ToList();
                    Task.Run(() =>
                    {
                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            this.commitcollection.AddRange(tmpCol);
                        });
                    });
                }
                catch (DirectoryNotFoundException ex)
                {
                    DispatcherHelper.CheckBeginInvokeOnUI(() =>
                    {
                        dialogService.ShowMessageBox(this, ex.Message);
                        //    MessengerInstance.Send(new ShowErrorMessage(ex.Message));
                    });

                }
                catch (Exception ex)
                {
                    DispatcherHelper.CheckBeginInvokeOnUI(() =>
                    {
                        dialogService.ShowMessageBox(this, ex.Message);
                    });
                }
                stopper.Stop();
                this.commitLoadTime = stopper.ElapsedMilliseconds;
                stopper = null;
            });
        }
        private List<ICommitItem> performModifyOperations(IEnumerable<ICommitItem> tmpCol)
        {
            // Filter Auswerten
            if (useExclude)
                tmpCol = FilterClass.ApplyExludeFilterList(tmpCol as List<ICommitItem>);
            if (useInclude)
                tmpCol = FilterClass.ApplyIncludeFilterList(tmpCol as List<ICommitItem>);
            if (HideToolFiles)
                tmpCol = FilterClass.ApplyExcludeWorkingFileFilterList(tmpCol as List<ICommitItem>);

            // Dokumentierte Commits in Liste übertragen
            tmpCol = docCommits.transferStateToFullList(tmpCol as List<ICommitItem>);

            // Substitutionen auswerten
            if (useSubs)
            {
                tmpCol = SubstitutionHandling.SubstituteMessages(tmpCol as List<ICommitItem>);
                tmpCol = SubstitutionHandling.SubstituteFileNames(tmpCol as List<ICommitItem>);
            }
            return tmpCol as List<ICommitItem>;
        }
        private void initButtonCommands()
        {
            this.DoWorkCommand = new RelayCommand(
                () => { WorkerDoWork(); },
                () =>
                {
                    if (Worker != null)
                        return !Worker.WorkerBusy;
                    else
                        return true;
                });

            this.SaveCommand = new RelayCommand(
                () =>
                {
                    doSaveCommitscommand();
                },
                () => { return commitcollection.Count > 0; });

            this.BrowseCommand = new RelayCommand(() => { SetWorkingPathFileBrowser(); }, () => { return true; });

            this.SaveSubCommand = new RelayCommand(() => { addSubToList(); }, () => { return true; });
            this.SaveSubListCommand = new RelayCommand(() => { safeSubList(); }, () => { return true; });
            this.DelSubCommand = new RelayCommand(() => { delSubFromList(); }, () => { return true; });

            this.SaveExcludeFilterCommand = new RelayCommand<string>(param => { addFilterToList(param); }, param => { return true; });
            this.DeleteExcludeFilterCommand = new RelayCommand<string>(param => { deleteFilterFromList(param); }, param => { return true; });
            this.SaveExcludeFilterListCommand = new RelayCommand(() => { FilterClass.SaveExcludeFilterToDisk(this.RepoPath); }, () => { return true; });

            this.SaveIncludeFilterCommand = new RelayCommand<string>(param => { addIncludeFilterToList(param); }, param => { return true; });
            this.DeleteIncludeFilterCommand = new RelayCommand<string>(param => { deleteIncludeFilterFromList(param); }, param => { return true; });
            this.SaveIncludeFilterListCommand = new RelayCommand(() => { FilterClass.SaveIncludeFilterToDisk(this.RepoPath); }, () => { return true; });

            this.EvalDirCommand = new RelayCommand<string>(param => { evaluateWorkingPath(param); }, param => { return true; });
            this.ExportCommand = new RelayCommand<string>(param => { exportListToCSV(param); }, param => { return true; });
            this.TestError = new RelayCommand(() => { });
            this.WindowClosing = new RelayCommand(() => { saveSettingsBeforeClose(); });
        }
        private void SetWorkingPathFileBrowser()
        {
            evaluateWorkingPath(OpenBrowser());
        }
        private string OpenBrowser()
        {
            string outString = "";
            var settings = new FolderBrowserDialogSettings
            {
                Description = "The new Working Path."
            };
            bool? success = dialogService.ShowFolderBrowserDialog(this, settings);
            if (success.HasValue && success.Value)
            {
                outString = settings.SelectedPath;
            }
            return outString;
        }
        private void exportListToCSV(string fileType)
        {
            //string msg = "";
            //if (CSVExport.export(commitcollection, Worker.BranchName, RepoPath, HideDocCommits))
            //{
            //    msg = "Export im Arbeitsverzeichnis erfolgt.";
            //    docCommits.SaveAllAsDocumented(this.commitcollection, this.User, this.VersionTag);
            //}
            //else
            //    msg = "Fehler im Export";
            if (String.IsNullOrEmpty(this.VersionTag))
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    dialogService.ShowMessageBox(this, "Version tag invalid. Please define a valid version number.");
                    SelectedTabIndex = 1;
                    return;
                });
            }

            string DefaultExtension = "";
            string DefaultFilter = "Excel 2007+|*.xlsx|PDF|*.pdf|CSV|*.csv";
            string saveLocation = "";
            ExportType type = getExportType(fileType);
            switch (fileType)
            {
                case "XLSX":
                    DefaultExtension = ".xlsx";
                    break;
                case "PDF":
                    DefaultExtension = ".pdf";
                    break;
                case "CSV":
                    DefaultExtension = ".csv";
                    break;
            }
            var settings = new SaveFileDialogSettings
            {
                Title = "Choose a save location.",
                InitialDirectory = Path.GetFullPath(RepoPath),
                FileName = getDefaultExportFileName(),
                DefaultExt = DefaultExtension,
                Filter = DefaultFilter,
                CheckFileExists = false
            };
            bool? success = dialogService.ShowSaveFileDialog(this, settings);
            if (success.Value)
            {
                saveLocation = settings.FileName;
                ExportService.ExportTo(
                    getExportType(Path.GetExtension(settings.FileName)),
                    saveLocation);
                docCommits.SaveAllAsDocumented(this.commitcollection, this.User, this.versionTag);
                docCommits.readDocumentedCommitsFromDisk();
                this.commitcollection = new SuspendableObservableCollection<ICommitItem>(docCommits.transferStateToFullList(this.commitcollection.ToList()));
                initCollectionSync();
            }
        }
        private string getDefaultExportFileName()
        {
            return String.Format($"changelog-" +
                $"{this.Worker.BranchName.Replace("/", "_").Replace("\\", "_")}-" +
                $"{this.StartDate.ToString("ddMM")}-" +
                $"{this.EndDate.ToString("ddMM")}");
        }
        private ExportType getExportType(string ext)
        {
            switch (ext)
            {
                case ".xlsx":
                case "XLSX":
                    return ExportType.XLSX;
                case ".pdf":
                case "PDF":
                    return ExportType.PDF;
                case ".csv":
                case "CSV":
                default:
                    return ExportType.CSV;
            }
        }
        private void UpdateResult(MessageBoxResult result)
        {
            switch (result)
            {
                case MessageBoxResult.OK:
                    Confirmation = "We got confirmation to continue!";
                    break;

                case MessageBoxResult.Cancel:
                    Confirmation = string.Empty;
                    break;

                default:
                    throw new NotSupportedException($"{Confirmation} is not supported.");
            }
        }
        private bool evaluateWorkingPath(object parameter)
        {
            var inpustring = (string)parameter;
            this.RepoPath = inpustring;
            try
            {
                this.BGCol = (util.isGitRepo(inpustring)) ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Tomato);
                this.UpdateFilterAndSubstitutionList();
                return true;

            }
            catch (Exception ex)
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    dialogService.ShowMessageBox(this, ex.Message);
                });
                return false;
            }
        }
        private void doSaveCommitscommand()
        {
            try
            {
                docCommits.SaveDocumentedCommits(this.commitcollection, this.User, this.VersionTag);
                docCommits.readDocumentedCommitsFromDisk();
                this.commitcollection = new SuspendableObservableCollection<ICommitItem>(docCommits.transferStateToFullList(this.commitcollection.ToList()));
                initCollectionSync();
            }
            catch (ArgumentException arg)
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    dialogService.ShowMessageBox(this, arg.Message);
                    SelectedTabIndex = 1;
                });
            }
        }
        private void saveSettingsBeforeClose()
        {
            Settings.Default.Save();
        }
        #region Filter Buttons
        private void deleteIncludeFilterFromList(string delFilter)
        {
            FilterClass.deleteIncludeFilterFromList(delFilter);
        }
        private void addIncludeFilterToList(string addFilter)
        {
            FilterClass.addIncludeFilterToList(addFilter);
        }
        private void deleteFilterFromList(string delFilter)
        {
            FilterClass.deleteExcludeFilterFromList(delFilter);
        }
        private void addFilterToList(string addFilter)
        {
            FilterClass.addExcludeFilterToList(addFilter);
        }
        #endregion
        #region Substitution Buttons
        private void addSubToList()
        {
            SubstitutionHandling.Add(SelectedSubstitutionItem);
            SelectedSubstitutionItem = new SubstitutionItem("", "");
        }
        private void delSubFromList()
        {
            if (FocusedSubItem != null)
                SubstitutionHandling.Remove(FocusedSubItem);
        }
        private void safeSubList()
        {
            SubstitutionHandling.SaveList(this.RepoPath);
        }
        private void UpdateFilterAndSubstitutionList()
        {
            if (!String.IsNullOrEmpty(this.RepoPath) && Directory.Exists(this.RepoPath))
            {
                if (FilterClass != null)
                {
                    FilterClass.WorkingPath = this.RepoPath;
                    //FilterClass.ReadExcludeFilterFromDisk(this.RepoPath);
                }
                if (SubstitutionHandling != null)
                    SubstitutionHandling.ReadSubstitutionsFromDisk(this.RepoPath);
            }
        }
        #endregion
        #endregion

        #region Properties
        public SuspendableObservableCollection<TreeItem> TreeItems { get; set; }
        public gitworker Worker { get; private set; }
        public SuspendableObservableCollection<ICommitItem> commitcollection
        {
            get => _commitcollection;
            set
            {
                if (value == _commitcollection)
                    return;
                _commitcollection = value;
                this.RaisePropertyChanged("commitcollection");
            }
        }
        public CommitItem SelectedItem
        {
            get => item;
            set
            {
                if (item == value)
                    return;
                item = value;
                this.RaisePropertyChanged("SelectedItem");
            }
        }
        public int SelectedTabIndex
        {
            get => _selectedTabIndex;
            set
            {
                if (_selectedTabIndex == value || value > 2)
                    return;
                _selectedTabIndex = value;
                this.RaisePropertyChanged("SelectedTabIndex");
            }
        }
        public string RepoPath
        {
            get => repopath;
            set
            {
                if (repopath == value)
                    return;
                repopath = value;
                Settings.Default.RepoPath = value;
                this.RaisePropertyChanged("RepoPath");
            }
        }
        public string WindowTitle
        {
            get { return "gitlogtool"; }
        }
        public DateTime EndDate
        {
            get => endDate;
            set
            {
                if (endDate == value)
                    return;
                endDate = value;
                this.RaisePropertyChanged("EndDate");
            }
        }
        public DateTime StartDate
        {
            get => startDate;
            set
            {
                if (startDate == value)
                    return;
                startDate = value;
                this.RaisePropertyChanged("StartDate");
            }
        }
        public SolidColorBrush BGCol
        {
            get => bgCol;
            set
            {
                if (bgCol == value)
                    return;
                bgCol = value;
                this.RaisePropertyChanged("BGCol");
            }
        }
        public gitUser User
        {
            get => user;
            set
            {
                if (user == value)
                    return;
                user = value;
                this.RaisePropertyChanged("User");
            }
        }
        public string VersionTag
        {
            get => versionTag;
            set
            {
                if (versionTag == value)
                    return;
                versionTag = value;
                Settings.Default.VersionTag = value;
                this.RaisePropertyChanged("VersionTag");
            }
        }
        public string IncludeFilterToAdd
        {
            get => _includeFilterToAdd;
            set
            {
                _includeFilterToAdd = value;
                this.RaisePropertyChanged("IncludeFilterToAdd");
            }
        }
        public string ExcludeFilterToAdd
        {
            get => _excludefilterToAdd;
            set
            {
                _excludefilterToAdd = value;
                this.RaisePropertyChanged("ExcludeFilterToAdd");
            }
        }
        public Filter FilterClass
        {
            get => filter;
            set
            {
                filter = value;
                this.RaisePropertyChanged("Filter");
            }
        }
        public SubstitutionList SubstitutionHandling
        {
            get => substitution;
            set
            {
                substitution = value;
                this.RaisePropertyChanged("Substitution");
            }
        }
        public SubstitutionItem SelectedSubstitutionItem
        {
            get => _selectedSubItem;
            set
            {
                _selectedSubItem = value;
                this.RaisePropertyChanged("SelectedSubstitutionItem");
            }
        }
        public SubstitutionItem FocusedSubItem
        {
            get => _focusedSubItem;
            set
            {
                _focusedSubItem = value;
                this.RaisePropertyChanged("SelectedSubstitutionItem");
            }
        }
        public bool useSubs
        {
            get => _useSubs;
            set
            {
                if (value == _useSubs)
                    return;
                _useSubs = value;
                this.RaisePropertyChanged("useSubs");
            }
        }
        public long commitLoadTime
        {
            get => _commitLoadTime;
            set
            {
                if (_commitLoadTime == value)
                    return;
                _commitLoadTime = value;
                RaisePropertyChanged("commitLoadTime");
            }
        }
        public bool ListHasItems
        {
            get => this.commitcollection.Count > 0;
        }
        public bool useExclude
        {
            get => _useExclude;
            set
            {
                if (value == _useExclude)
                    return;
                _useExclude = value;
                this.RaisePropertyChanged("useExclude");
            }
        }
        public bool useInclude
        {
            get => _useInclude;
            set
            {
                if (value == _useInclude)
                    return;
                _useInclude = value;
                this.RaisePropertyChanged("useInclude");
            }
        }
        public bool activateDebugProp
        {
            get
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }

        public string Confirmation
        {
            get => confirmation;
            private set => Set(() => Confirmation, ref confirmation, value);
        }
        public bool HideDocCommits
        {
            get => _hideDocCommits;
            set
            {
                if (value == _hideDocCommits)
                    return;
                _hideDocCommits = value;
                this.RaisePropertyChanged("HideDocCommits");
            }
        }
        public bool HideToolFiles
        {
            get => _hideToolFiles;
            set
            {
                if (value == _hideToolFiles)
                    return;
                _hideToolFiles = value;
                this.RaisePropertyChanged("HideToolFiles");
            }
        }

        public IServiceContainer ServiceContainer
        {
            get
            {
                if (serviceContainer == null)
                    serviceContainer = new ServiceContainer(this);
                return serviceContainer;
            }
        }
        IServiceContainer ISupportServices.ServiceContainer { get { return ServiceContainer; } }
        #endregion
    }
}