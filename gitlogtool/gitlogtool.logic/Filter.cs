﻿using miniLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace gitlogtool.logic
{
    public class Filter : INotifyPropertyChanged
    {
        #region Member
        private ObservableCollection<string> _excludefilterList;
        private ObservableCollection<string> _includefilterList;
        private string excludeFileName = Constants.EXCLUDEFILTERLIST;
        private string includeFileName = Constants.INCLUDEFILTERLIST;
        private string workingPath = "";
        #endregion
        #region Interface
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region Constructor
        public Filter()
        {
            IncludeFilterList = new ObservableCollection<string>();
            ExcludeFilterList = new ObservableCollection<string>();
        }
        #endregion
        #region Properties
        public ObservableCollection<string> ExcludeFilterList
        {
            get => _excludefilterList;
            set
            {
                if (_excludefilterList == value)
                    return;
                _excludefilterList = value;
                OnPropertyChanged("ExcludeFilterList");
            }
        }
        public ObservableCollection<string> IncludeFilterList
        {
            get => _includefilterList;
            set
            {
                if (_includefilterList == value)
                    return;
                _includefilterList = value;
                OnPropertyChanged("IncludeFilterList");
            }
        }
        public string WorkingPath
        {
            get => workingPath;
            set
            {
                if (value == workingPath)
                    return;
                workingPath = value;
                OnPropertyChanged("FilterList");
                this.ReadExcludeFilterFromDisk(this.WorkingPath);
                this.ReadIncludeFilterFromDisk(this.WorkingPath);
            }
        }
        #endregion
        #region File Handling
        public bool? SaveExcludeFilterToDisk(string workingdir)
        {
            if (String.IsNullOrEmpty(workingdir))
                return false;

            try
            {
                using (StreamWriter file = new StreamWriter(Path.Combine(workingdir, excludeFileName), append: false))
                {
                    foreach (var item in ExcludeFilterList)
                    {
                        file.WriteLine(item);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool? SaveIncludeFilterToDisk(string workingdir)
        {
            if (String.IsNullOrEmpty(workingdir))
                return false;

            try
            {

                using (StreamWriter file = new StreamWriter(Path.Combine(workingdir, includeFileName), append: false))
                {
                    foreach (var item in IncludeFilterList)
                    {
                        file.WriteLine(item);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public void ReadExcludeFilterFromDisk(string workingdir)
        {
            Task.Run(() =>
            {
                if (File.Exists(Path.Combine(workingdir, excludeFileName)))
                {
                    ExcludeFilterList = new ObservableCollection<string>(File.ReadAllLines(Path.Combine(workingdir, excludeFileName)));
                }
                else
                {
                    ExcludeFilterList = new ObservableCollection<string>();
                }
            });
        }
        public void ReadIncludeFilterFromDisk(string workingdir)
        {
            Task.Run(() =>
            {
                if (File.Exists(Path.Combine(workingdir, includeFileName)))
                {
                    IncludeFilterList = new ObservableCollection<string>(File.ReadLines(Path.Combine(workingdir, includeFileName)));
                }
                else
                {
                    IncludeFilterList = new ObservableCollection<string>();
                }
            });
        }
        #endregion
        #region Logic
        public List<ICommitItem> ApplyExludeFilterList(List<ICommitItem> items)
        {
            if (ExcludeFilterList == null)
                return new List<ICommitItem>(items);
            if (ExcludeFilterList.Count == 0)
                return new List<ICommitItem>(items);

            List<ICommitItem> outlist = new List<ICommitItem>();

            foreach (var item in items)
            {
                bool found = false;
                string message = item.CommitMessage.ToLower();
                foreach (var filter in ExcludeFilterList)
                {
                    if (message.Contains(filter.ToLower()))
                        found = true;
                    if (found)
                        break;
                }
                if (!found)
                    outlist.Add(item);
            }

            return outlist;
        }
        public List<ICommitItem> ApplyExcludeWorkingFileFilterList(List<ICommitItem> items)
        {
            List<ICommitItem> outlist = new List<ICommitItem>();

            foreach (var item in items)
            {
                bool found = false;
                string filename = item.CommitFileName.ToLower();
                foreach (var filter in Constants.FILENAMELIST)
                {

                    if (filename.Equals(filter.ToLower()))
                        found = true;
                    if (found)
                        break;
                }
                if (!found)
                    outlist.Add(item);
            }
            return outlist;
        }
        public List<ICommitItem> ApplyIncludeFilterList(List<ICommitItem> commits)
        {
            if (IncludeFilterList == null)
                return new List<ICommitItem>(commits);
            if (IncludeFilterList.Count == 0)
                return new List<ICommitItem>(commits);

            List<ICommitItem> outList = new List<ICommitItem>();

            foreach (var item in commits)
            {
                bool found = false;
                string message = item.CommitMessage.ToLower();
                foreach (var filter in IncludeFilterList)
                {
                    if (message.Contains(filter.ToLower()))
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    outList.Add(item);
            }
            return outList;
        }
        #endregion
        #region List Handling
        public bool addExcludeFilterToList(string filter)
        {
            if (String.IsNullOrEmpty(filter))
                return false;
            if (this.ExcludeFilterList.Contains(filter))
                return false;
            else
                this.ExcludeFilterList.Add(filter);
            return true;
        }
        public bool deleteExcludeFilterFromList(string filter)
        {
            if (this.ExcludeFilterList.Contains(filter))
            {
                this.ExcludeFilterList.Remove(filter);
                return true;
            }
            return false;
        }

        public bool deleteIncludeFilterFromList(string delFilter)
        {
            if (this.IncludeFilterList.Contains(delFilter))
            {
                this.IncludeFilterList.Remove(delFilter);
                return true;
            }
            return false;
        }

        public bool addIncludeFilterToList(string addFilter)
        {
            if (String.IsNullOrEmpty(addFilter))
                return false;
            if (this.IncludeFilterList.Contains(addFilter))
                return false;
            else
                this.IncludeFilterList.Add(addFilter);
            return true;
        }
        #endregion
    }
}
