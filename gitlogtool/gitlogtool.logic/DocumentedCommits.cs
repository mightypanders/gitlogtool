﻿using miniLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace gitlogtool.logic
{
    public class DocumentedCommits : INotifyPropertyChanged, IDisposable
    {
        #region Member
        private List<CommitItem> documentedCommitList;
        private string workingPath;
        private string documentFileName = Constants.DOCUMENTEDCOMMITS;
        private string versionTag = "";
        private gitUser docUser;
        #endregion

        #region Accessoren
        /// <summary>
        /// The List of all commits that have been documented. Filled with pseudo Commit Items, not to be show to the user.
        /// CommitItems carry CommitSHA, FilePath, DocumentationUser, DocumentationDate
        /// </summary>
        public List<CommitItem> DocumentedCommitList
        {
            get => documentedCommitList;
            set
            {
                if (documentedCommitList == value)
                    return;
                documentedCommitList = value;
                OnPropertyChanged("DocumentedCommitList");
            }
        }
        public string WorkingPath
        {
            get => workingPath;
            set
            {
                if (workingPath == value)
                    return;
                workingPath = value;
                OnPropertyChanged("WorkingPath");
            }
        }
        #endregion

        #region Interface
        public void Dispose()
        {
            this.documentedCommitList = null;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region Constructor
        /// <summary>
        /// Sets Working Path, Initializes DocumentedCommmitList, tries to read a documented Commit file, if present.
        /// </summary>
        /// <param name="_workingPath">The folder in which the documented Commits should be saved. The Working Directory</param>
        public DocumentedCommits(string _workingPath)
        {
            WorkingPath = _workingPath;
            DocumentedCommitList = new List<CommitItem>();
            documentedCommitList = readDocumentedCommitsFromDisk();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Saves all marked as documtend commits to the file .documentedCommits in the working directory
        /// </summary>
        /// <param name="_commitList">The shown CommitList in the User Interface</param>
        /// <param name="user">The current user. Will be saved to the file as well</param>
        /// <param name="_versionTag">documents which documentation version is being written.</param>
        /// <returns>
        /// True: File has been successfully written to disk
        /// False: user is null, version is null, exception during write
        /// </returns>
        public bool? SaveDocumentedCommits(SuspendableObservableCollection<ICommitItem> _commitList, gitUser user, string _versionTag)
        {
            if (user == null)
                return false;
            docUser = user;
            if (String.IsNullOrEmpty(_versionTag))
                throw new ArgumentException("Version tag invalid. Please define a valid version number.", _versionTag);
            versionTag = _versionTag;

            /* get all the paths of files that have been marked as documented
            * if an item has been checked
            * and if it is a pseudoitem
            * and (if it does not have a documentation version
            * or if the documentation date is 1.1.0001)
            */
            var documentedFileNames = _commitList.Where(
                x => x.CommitDocumented.HasValue
                && x.CommitDocumented.Value
                // PseudoCommit Items have invalid CommitSHAs to identify them
                && !x.validItem
                && (String.IsNullOrEmpty(x.DocumentedVersion)
                || x.DocumentedDate == new DateTime(0001, 1, 1))
                ).Select(x => x.CommitFileFullPath);

            /*
             * if an item has been checked
             * and if it is not a pseudoitem
             * and (if it does not have a documenation version
             * or if the documentation date is 1.1.0001)
             * or (if it is not pseudoitem
             * and the file list contains the path of the item)
             */
            var selectedCommitList = _commitList.Where(
                x => (x.CommitDocumented.HasValue
            && x.CommitDocumented.Value
            // Folder Structure Pseudoommits don't have a valid CommitSHA and can be discarded
            && x.validItem
            && (String.IsNullOrEmpty(x.DocumentedVersion)
            || x.DocumentedDate == new DateTime(0001, 1, 1)))
            || x.validItem
            && documentedFileNames.Contains(x.CommitFileFullPath)
            ).ToList();

            try
            {
                SaveCommitsToDisk(selectedCommitList);
            }
            catch (Exception)
            {
                return false;
            }

            DocumentedCommitList = readDocumentedCommitsFromDisk();

            return true;
        }
        public bool? SaveAllAsDocumented(SuspendableObservableCollection<ICommitItem> _commitList, gitUser user, string _versionTag)
        {
            foreach (var item in _commitList)
            {
                item.CommitDocumented = true;
            }

            return SaveDocumentedCommits(_commitList, user, _versionTag);
        }
        /// <summary>
        /// Compares the local commit list with the passed on and returns the passed one after setting all matching commits to "documented = true"
        /// </summary>
        /// <param name="completeModifiedList">The list that is to be modified</param>
        /// <returns>
        /// The passed in list, every commit that has been matched to a documented commit, has been updated with the matching information.
        /// </returns>        
        public List<ICommitItem> transferStateToFullList(List<ICommitItem> completeModifiedList)
        {
            foreach (var item in DocumentedCommitList)
            {
                completeModifiedList.Where(
                    x => x.CommitSHA.Equals(item.CommitSHA)
                    && x.CommitFileFullPath.Equals(item.CommitFileFullPath)).
                    Select(c =>
                    {
                        c.CommitDocumented = item.CommitDocumented;
                        c.DocumentedDate = item.DocumentedDate;
                        c.DocumentedVersion = item.DocumentedVersion;
                        c.DocumentingUser = item.DocumentingUser;
                        return true;
                    }).ToList();
            }
            return completeModifiedList;

        }
        #endregion
        #region Helper
        private string documentString(CommitItem item)
        {
            /*
             * File Structure
             * CommitSHA|DocumentedVersion|CommitFileFullPath|DocumentingUser|DocumentedDate
             */
            string sep = "|";
            return item.CommitSHA + sep + versionTag + sep + item.CommitFileFullPath + sep + docUser + sep + DateTime.Now.ToString();
        }
        #endregion
        #region Filehandling
        /// <summary>
        /// Reads the documented commits from the working directory
        /// </summary>
        /// <returns>
        /// A list of CommitItems, not to be shown to the user.
        /// Reference Items with Commit SHA, FilePath and the Documented Flag = true
        /// </returns>
        public List<CommitItem> readDocumentedCommitsFromDisk()
        {
            /*
             * File Structure
             * CommitSHA|DocumentedVersion|CommitFileFullPath|DocumentingUser|DocumentedDate
             */
            List<CommitItem> tmpList = new List<CommitItem>();
            if (File.Exists(Path.Combine(workingPath, documentFileName)))
            {
                var lines = File.ReadAllLines(Path.Combine(workingPath, documentFileName));
                foreach (var item in lines)
                {
                    if (item == "")
                        continue;
                    var chunks = item.Split('|');

                    CommitItem tmp = new CommitItem()
                    {
                        CommitSHA = chunks[0],
                        CommitDocumented = true,
                        CommitFileName = Path.GetFileName(chunks[2]),
                        CommitFilePath = Path.GetDirectoryName(chunks[2]),
                        DocumentingUser = chunks[3],
                        DocumentedVersion = chunks[1],
                        DocumentedDate = DateTime.Parse(chunks[4])
                    };
                    tmpList.Add(tmp);
                }
            }
            return tmpList;
        }
        private bool SaveCommitsToDisk(List<ICommitItem> _commitList)
        {
            using (System.IO.StreamWriter file = new StreamWriter(Path.Combine(workingPath, documentFileName), append: true))
            {
                foreach (CommitItem item in _commitList)
                {
                    file.WriteLine(documentString(item));
                }
            }
            return true;
        }
        #endregion
    }
}
